﻿using System;
using System.Net;
using System.Net.Sockets;
using System.IO;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace JsonRPC
{
	/// <summary>
	/// Json RPC client.
	/// </summary>
	public class JsonRPCClient : IDisposable
	{
		private TcpClient client;
		private IPEndPoint serverAddress;
		private object communicationLock = new object();
		public int RequestsID { get; private set; }

		private bool keepConnection;
		public bool KeepConnection {
			get { return keepConnection; }
			set { 
				if (!value && client.Connected)
					client.Close ();
				else {
					client = new TcpClient ();
					client.Connect (serverAddress);
				}
				keepConnection = value;
			}
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="JsonRPC.JsonRPCClient"/> class.
		/// </summary>
		/// <param name="endpoint">Endpoint.</param>
		public JsonRPCClient(IPEndPoint endpoint)
		{
			serverAddress = endpoint;
			client = new TcpClient ();
			client.Connect (serverAddress);
			RequestsID = 1;
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="JsonRPC.JsonRPCClient"/> class.
		/// </summary>
		/// <param name="ip">Ip.</param>
		/// <param name="port">Port.</param>
		public JsonRPCClient(IPAddress ip, int port): this (new IPEndPoint (ip, port))
		{
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="JsonRPC.JsonRPCClient"/> class.
		/// </summary>
		/// <param name="host">Host.</param>
		/// <param name="port">Port.</param>
		public JsonRPCClient(string host, int port) : this (IPAddress.Parse (host), port)
		{
		}

		/// <summary>
		/// Calls the remote procedure "methodName", returning void, with specified argumants.
		/// </summary>
		/// <param name="methodName">Method name.</param>
		/// <param name="args">Arguments.</param>
		/// <exception cref="Exception">Throws exception when remote function throwed it. See ex.Message for details</exception>
		public void CallRPC(string methodName, params object[] args)
		{
			if (!KeepConnection) {
				client = new TcpClient ();
				client.Connect (serverAddress);
			}
			try{
				lock (communicationLock) 
				{
					string request = JObject.FromObject (new {
						method = methodName,
						@params = args,
						id = RequestsID
					}).ToString (Formatting.None);

					var writer = new StreamWriter (client.GetStream ());
					writer.WriteLine (request);
					writer.Flush ();

					var reader = new StreamReader (client.GetStream ());
					string answer = reader.ReadLine ();
					var answerObject = JObject.Parse (answer);

					JToken error = null;
					if (answerObject.TryGetValue ("error", out error)) {
						throw new Exception (answerObject["error"]["message"] + " : " + answerObject["error"]["data"]);
					}
				}
			}
			finally {
				if (!KeepConnection) client.Close ();
			}
		}

		/// <summary>
		/// Calls the remote procedure "methodName", returning value of type T, with specified argumants.
		/// </summary>
		/// <returns>Value which is returned by function</returns>
		/// <param name="methodName">Method name.</param>
		/// <param name="args">Arguments.</param>
		/// <typeparam name="T">Simple type: bool, int, string, float and etc. (simple for json serialization)</typeparam>
		/// <exception cref="Exception">Throws exception when remote function throwed it. See ex.Message for details</exception>
		public T CallRPC<T>(string methodName, params object[] args)
		{
			if (!KeepConnection) {
				client = new TcpClient ();
				client.Connect (serverAddress);
			}
			try{
				lock (communicationLock) 
				{
					string request = JObject.FromObject (new {
						method = methodName,
						@params = args,
						id = RequestsID
					}).ToString (Formatting.None);

					var writer = new StreamWriter (client.GetStream ());
					writer.WriteLine (request);
					writer.Flush ();

					var reader = new StreamReader (client.GetStream ());
					string answer = reader.ReadLine ();
					var answerObject = JObject.Parse (answer);

					JToken error = null;
					if (answerObject.TryGetValue ("error", out error)) {
						throw new Exception (answerObject ["error"] ["message"] + " : " + answerObject ["error"] ["data"]);
					}

					var value = answerObject.GetValue ("result");
					return (value != null) ? value.Value<T> () : default(T);
				}
			}
			finally {
				if (!KeepConnection) client.Close ();
			}
		}

		/// <summary>
		/// Releases all resource used by the <see cref="JsonRPC.JsonRPCClient"/> object.
		/// </summary>
		/// <remarks>Call <see cref="Dispose"/> when you are finished using the <see cref="JsonRPC.JsonRPCClient"/>. The
		/// <see cref="Dispose"/> method leaves the <see cref="JsonRPC.JsonRPCClient"/> in an unusable state. After calling
		/// <see cref="Dispose"/>, you must release all references to the <see cref="JsonRPC.JsonRPCClient"/> so the garbage
		/// collector can reclaim the memory that the <see cref="JsonRPC.JsonRPCClient"/> was occupying.</remarks>
		public void Dispose(){
			if (client.Connected) client.Close ();
		}
	}
}

