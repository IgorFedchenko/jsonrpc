﻿using System;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Collections.Generic;
using System.Threading;

namespace JsonRPC
{
	/// <summary>
	/// Json RPC server.
	/// </summary>
	public class JsonRPCServer
	{
		private static List<JsonRpcService> services = new List<JsonRpcService>();
		private static AsyncCallback rpcResultHandler;
		private static TcpListener server;
		private static Thread receivingThread = null;

		/// <summary>
		/// Initializes a listeinig server on specified address and port
		/// </summary>
		/// <param name="localAddr">Local address.</param>
		/// <param name="listenPort">Listen port.</param>
		public JsonRPCServer(IPAddress localAddr, int listenPort)
		{
			server = new TcpListener (localAddr, listenPort);

			rpcResultHandler = new AsyncCallback (
				state => {
					var async = ((JsonRpcStateAsync)state);
					var result = async.Result;
					var writer = ((StreamWriter)async.AsyncState);

					writer.WriteLine (result);
					writer.Flush ();
				}
			);
		}

		/// <summary>
		/// Start the specified JsonRpcServices to handle their JsonRpcMethods.
		/// </summary>
		/// <param name="_services">Services.</param>
		public void Start(params JsonRpcService[] _services){
			services.AddRange (_services);
			server.Start ();
			receivingThread = new Thread (AcceptClientLoop);
			receivingThread.IsBackground = true;
			receivingThread.Start ();
		}

		/// <summary>
		/// Stop this server.
		/// </summary>
		public void Stop(){
			receivingThread.Abort ();
			server.Stop ();
		}

		private void AcceptClientLoop()
		{
			while (true)
			{
				try{
					var client = server.AcceptTcpClient ();
					ThreadPool.QueueUserWorkItem (new WaitCallback(state => {
						try
						{
							var stream = client.GetStream();
							var reader = new StreamReader(stream, Encoding.UTF8);
							var writer = new StreamWriter(stream, new UTF8Encoding(false));

							while (!reader.EndOfStream)
							{
								var line = reader.ReadLine();
								var async = new JsonRpcStateAsync(rpcResultHandler, writer) { JsonRpc = line };
								JsonRpcProcessor.Process(async, writer);
							}
						}
						catch (Exception ex) { 
							File.AppendAllText ("ERROR_SERVER.txt", ex.Message + "\n" + ex.StackTrace);
						}
					}));
				}
				catch (Exception ex){
					File.AppendAllText ("ERROR_SERVER.txt", ex.Message + "\n" + ex.StackTrace);
				}
			}
		}

	}
}
