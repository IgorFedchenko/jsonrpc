﻿using System;
using System.Threading;

namespace JsonRPC
{
	public class Task
	{
		public static int? CurrentId {
			get{
				if (!Thread.CurrentThread.IsThreadPoolThread || !Thread.CurrentThread.IsBackground)
					return null;
				else
					return Thread.CurrentThread.ManagedThreadId;
			}
		}
	}
}

