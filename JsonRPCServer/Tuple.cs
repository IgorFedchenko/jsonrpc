﻿using System;

namespace JsonRPC
{
	public class Tuple<T1, T2>
	{
		public T1 Item1 { get; private set; }
		public T2 Item2 { get; private set; }
		public Tuple(T1 first, T2 second)
		{
			Item1 = first;
			Item2 = second;
		}
	}

	public class Tuple<T1, T2, T3>
	{
		public T1 Item1 { get; private set; }
		public T2 Item2 { get; private set; }
		public T3 Item3 { get; private set; }
		public Tuple(T1 first, T2 second, T3 third)
		{
			Item1 = first;
			Item2 = second;
			Item3 = third;
		}
	}

	public static class Tuple{
		public static Tuple<T1, T2> Create<T1, T2>(T1 item1, T2 item2){
			return new Tuple<T1, T2> (item1, item2);
		}
	}
}

