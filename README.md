# JsonRPC #

This solution contains both sides of rpc commutation:

* JsonRPCServer - library for building server-side application
* JsonRPCClient - library for building client-side application

## JsonRPCServer ##
This project is made of JSON-RPC.NET project. The main difference is that this project has JsonRPCServer class, which manages socket connection, client receiving and requests handling registration for you. So, instead of looking at example project here (https://github.com/Astn/JSON-RPC.NET/wiki/Getting-Started-%28Sockets%29), you can just write the following two lines:

```
#!c#
//Initialize server on  port 14400 - like TcpClient object
JsonRPCServer rpcServer = new JsonRPCServer (IPAddress.Any, 14400);

//Run server with instanses of JsonRpcServices used to manage requests
rpcServer.Start (new ScannersManager());
```
It starts it's own thread for waiting and handling clients in asynchronous mode. For more details about how this library works, look at the sources and take this documentation: https://github.com/Astn/JSON-RPC.NET/wiki

## JsonRPCClient ##
This is a simple implementation of client-side for json-rpc protocol. It makes the following:

1. Serializes function calls to json
2. Sends json to server and get's json answer
3. Deserializes answer
4. Returns the result or rises the same exception as was thrown on server while handling the request

To use this library you need just connect JsoinRPCClient instance to remote server and call CallRPC generic function (or just CallRPC function when getting void result). 
For now only simple types like bool, int, string and float supported.

Sample usage is the following:

```
#!c#
//Connect to JSON-RPC server on localhost 14400 port
using (var rpcClient = new JsonRPCClient ("127.0.0.1", 14400)) {
    rpcClient.CallRPC ("remote_method_name", "first_param", 0, "third_param");   // calling function returning void
    string answer = rpcClient.CallRPC<string> ("remote_method_returning_string");  // calling function returning string
    try{
        rpcClient.CallRPC("throws_exception_method");
    }
    catch {
        // Handling remote thrown exception
    }
}

```